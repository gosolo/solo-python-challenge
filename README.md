# Python Code Challenge

To get started, please download this repository rather than forking it.

## Challenge Instructions

For this challenge, we are asking you to implement a Quick Sort algorithm.

Write a function that takes in an array of integers and returns a sorted version of that array, using Quick Sort.

If you are unfamiliar with Quick Sort, we recommend watching [this video](https://www.geeksforgeeks.org/quick-sort/).

### Requirements

Your submission will be evaluated based on the following:

1. Functionality - your code correctly executes QuickSort.
2. Organization/Best Practices - your code is easy to read and understand by other developers.

Extra Credit:

1. Unit Tests
2. Documentation - Please provide some documentation for your tests, as well as explanations for any technical decisions you made..

## Other

If you are brought in for an in-person interview, you will be asked questions about your submission to ensure you understand your implementation. Please do not just copy and paste a StackOverflow answer. You won't be doing yourself any favors.

When you are done, please zip up your final submission and send it to your recruiter.

If you have any questions, please let us know.


